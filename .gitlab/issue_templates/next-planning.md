**Add issue title: Enablement Section NEXT Prioritization - [%release]**

## Background
Please refer to https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/

## Prerequisites
Review prerequisites and then answer the questions
1. Issue type dashboard: https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1
1. MR type dashboard: https://10az.online.tableau.com/#/site/gitlab/views/DevelopmentEmbeddedDashboard_17017859046500/DevelopmentEmbeddedDashboard?:iid=1
1. Planning issues for [%release]

## Questions to answer
### Context:

1. What groups/sections does this review cover?
1. Is the dashboard accurate? Are the number of issues in the dashboard within 5% of the SSOT (the issues themselves)? If not, what needs to be done to correct?

### Maintenance/quality:

1. Are the % of undefined issues < 1% for the timeframe being analyzed? If not, what should be done to correct?
1. Are the % of undefined merge requests < 1% for the timeframe being analyzed? If not, what should be done to correct?
1. How do error budgets look?
1. Are past due bugs being prevented/prioritized as appropriate?
1. Are usability/SUS impacting issues being prioritized as appropriate?
1. Are security issues being prioritized as appropriate?
1. Are infrastructure backlog issues being prioritized as appropriate?
1. Are maintenance priorities from the engineering development manager being prioritized as appropriate?

### Features:

1. How is the group addressing the product investment themes?
1. How do revenue drivers impact plans for this group?
1. How are customer requests being addressed for this group?

### Trends:

1. Evaluate the percentage ratios of completed work (feature / maintenance / bug) for the previous milestone/timeframe against the team's planned ratio for that milestone.
1. Is there predictability from milestone to milestone (number of issues or issue weight per release)?
1. Compare the planned milestone with the previous months merge request trends for the team. Any trends to note?
1. What overall trends does the group want to highlight?
1. What flags do you want to raise? What won't happen?


## Action Items

**Quad**: Please check off if your team (Product, Development, QE, UX) has gone through the above tasks and is aligned to the release priorities.

- [ ] Database: @alexives (Quad: @rogerwoo  - PM, @alexives - Dev , @ksvoboda - QE, N/A - UX ) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] Geo: @pjphillips (Quad: @sranasinghe - PM, @pjphillips - Dev , @ksvoboda  - QE, N/A - UX ) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] Gitaly:Cluster: @andrashorvath (Quad: @mjwood  - PM, @andrashorvath  - Dev , @ksvoboda - QE, N/A - UX ) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] Gitaly:Git: @jcaigitlab (Quad: @mjwood  - PM, @jcaigitlab - Dev , @ksvoboda - QE, N/A - UX ) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] Global Search: @changzhengliu (Quad: @bvenker  - PM, @changzhengliu  - Dev , @ksvoboda  - QE, N/A - UX ) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] Cloud Connector: @pjphillips (Quad: @rogerwoo - PM, @pjphillips - Dev , @ksvoboda - QE, N/A - UX) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] Tenant Scale: @sissiyao (Quad: @lohrc - PM, @sissiyao - Dev , @ksvoboda - QE, @mnichols1 - UX ) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] Distribution:Build: @sissiyao (Quad: @dorrino  - PM, @sissiyao - Dev , @ksvoboda  - QE, N/A - UX ) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] Distribution:Deploy : @plu8  (Quad: @dorrino  - PM, @plu8 - Dev , @ksvoboda  - QE, N/A - UX ) 
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)
- [ ] DBRE: @rmar1  
   - [ ] Add link to [planning issue]()
   - [ ] [Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/views/DRAFTIssueTypesDetail/IssuesbyGroup?:iid=1) - please confirm filter is set to the group correctly and `undefined` type is [less than 1%](https://about.gitlab.com/handbook/engineering/cross-functional-prioritization/#:~:text=Are%20the%20%25%20of%20undefined%20issues%20%3C%201%25%20for%20the%20timeframe%20being%20analyzed%3F%20If%20not%2C%20what%20should%20be%20done%20to%20correct%3F)

## Additional Notes
1. Groups can decide whether they want to do Sync or Async review. Once review is completed, check off above.
1. If any group has additional notes to share, please add a comment in this issue.

/assign @sissiyao @rmar1 @pjphillips @plu8 @andrashorvath @jcaigitlab @changzhengliu @alexives 
